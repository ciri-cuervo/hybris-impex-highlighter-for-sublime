# Installation #

Download last version of the ImpEx Highlighter: [impex.tmLanguage](https://bitbucket.org/ciri-cuervo/hybris-impex-highlighter-for-sublime/downloads/impex.tmLanguage)

Place the downloaded file in the following directory:

### Sublime Text 2 ###

    OS X: ~/Library/Application Support/Sublime Text 2
    Windows: %APPDATA%\Sublime Text 2
    Linux: ~/.config/sublime-text-2


### Sublime Text 3 ###

    OS X: ~/Library/Application Support/Sublime Text 3
    Windows: %APPDATA%\Sublime Text 3
    Linux: ~/.config/sublime-text-3
